===============

# Note version release

## Giá trị biến cần thiết trong file values.yaml

| Variables | type | default | description |
|-|:-:|:-:|-|
| image.repository | string | "" | Link dẫn tới images pull |
| image.pullPolicy | string  | "" | Chính sách pull |
| image.containerPort  | number | null | Port listen của container |
| image.containerPortName  | string  | "" | Tên cho port |
| image.command  | array | [] | command thực thi sau khi run container |
| image.args | array | [] | tham số truyền vào cho command |
| fluentd.enabled | bool  | false | Enable cài đặt pod thêm container fluentd |
| fluentd.name  | string | "" | Tên của image |
| fluentd.image  | string  | "" | Link dẫn tới images pull fluentd |
| fluentd.env | array | [] | Danh sách biến môi trường |
| fluentd.env.name | string | "" | Tên biến môi trường |
| fluentd.env.value | string | "" | Gía trị của biến môi trường |
| nameOverride | string | "" | Tên dùng trong template |
| fullnameOverride | string  | "" | Full tên dùng trong template |
| service.type | string | ClusterIP | Loại service, NodePort, ClusterIP, LoadBalancer |
| service.port  | number  | 4000 | Port của dịch vụ sẽ run |
| ingress.enabled  | boolen | false | Enable sử dụng ingress |
| ingress.annotations | map(string) | kubernetes.io/ingress.class: nginx | annotations cho ingress |
| ingress.hosts  | array | [] | chứ các host và các paths đường dẫn |
| ingress.tls | array | [] | Khai báo dùng tls cho ingress |
| imagePullSecrets | array | - name: image-pull-secret-sharesys | create and declare imagePullSecrets for project |
| additionalSSLCert.enabled | boolean | false | Enable additional SSL Cert |
| additionalSSLCert.tls | map | {} | Truyền nội dung file cert và key theo base64 encode vào |

## Version 2.5.1

- Add tty support

## Version 2.5.0

- Add argo rollout

## Version 2.4.2

- Move CPU hpa to bellow Memory

## Version 2.4.1

- Fix Destination Rules dynamic enable

## Version 2.4.0

- Move gateway definition to virtualservice helm value, add [IMAGE_NAME_CI]

## Version 2.3.0

- Add destination rule with trafficPolicy (connection pool, persistent client, vv)

## Version 2.2.2

- Change cors to yaml ( must include cors in value file)

## Version 2.2.1

- Add rewrite istio (if have value)

## Version 2.2.0

- Add NetworkPolicy Ingress

## Version 2.1.1

- Add sealed-secrets from env and files

## Version 2.0.5

- fix ssl istio

## Version 2.0.4

- Add Istio vs, gw, ef, timeout and retry

## Version 0.1.4

- Add imagePullSecrets

## Version 0.1.3 - 0.1.8

- Fix bug dynamic fluent image. Fluentd container khi này là tham số linh động, set fluentd.enabled là true trong values.yaml để có thể dùng
- Fix bug dynamic command.
  - Khi truyền vào image.command và image.args sẽ run container kèm với CMD
  - image.command: Command sẽ thực hiện
  - image.args: Tham số truyền vào command

## Version 0.1.2

- Run ổn định
- Run với 1 image tạo được 1 container trong pod
- Fix health check liveness và readiness Probe, tăng 10s timeout
